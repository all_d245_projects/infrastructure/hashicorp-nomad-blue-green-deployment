bind_addr = "0.0.0.0"
log_level = "info"

client {
  enabled             = true
  network_interface   = "{{ GetDefaultInterfaces | attr \"name\" }}"
}

plugin "docker" {
    config {
      volumes {
        enabled = true
      }
      gc {
        image_delay = "12h"
      }
    }
  }
