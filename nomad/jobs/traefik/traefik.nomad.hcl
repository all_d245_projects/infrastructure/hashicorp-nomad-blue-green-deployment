variable "traefik_web_port" {
  type = number
}
variable "traefik_api_port" {
  type = number
}

job "traefik" {
  type        = "service"

  group "traefik" {
    update {
      auto_revert = true
    }

    network {
      port "web" {
        to     = 80
        static = "${var.traefik_web_port}"
      }
      port "api" {
        to     = 8080
        static = "${var.traefik_api_port}"
      }
    }

    service {
      provider = "nomad"
      name = "traefik-web"
      port = "web"
      check {
        name     = "traefik-web http port" 
        type     = "tcp"
        interval = "10s"
        timeout  = "5s"
      }
    }

    service {
      provider = "nomad"
      name = "traefik-api"
      port = "api"
      check {
        name = "traefik api ping"
        type     = "http"
        path     = "/ping"
        interval = "10s"
        timeout  = "5s"
      }
    }

    task "traefik" {
      driver = "docker"
      config {
        image = "docker.io/library/traefik:v3.0"
        ports = ["web", "api"]
        args = [
          "--configFile=${NOMAD_TASK_DIR}/config/traefik.yml"
        ]
      }
      template {
        data = file("/project/nomad/jobs/traefik/templates/traefik-static-config.yml")
        destination = "${NOMAD_TASK_DIR}/config/traefik.yml"
      }
      env = {
        NOMAD_ADDR  = "http://${NOMAD_HOST_IP_web}:4646"
      }
    }
  }
}
