packer {
  required_plugins {
    ansible = {
      source  = "github.com/hashicorp/ansible"
      version = "~> 1"
    }
  }
}

source "null" "null" {
  communicator = "none"
}

build {
  name        = "traefik:deploy"
  description = "Deploys an instance of Traefik within the Nomad cluster."
  sources     = ["null.null"]

  provisioner "shell-local" {
    command = "nomad job run /project/nomad/jobs/traefik/traefik.nomad.hcl"
    env = {
      NOMAD_ADDR                 = "http://${var.nomad_host}:${var.nomad_port}"
      NOMAD_VAR_traefik_web_port = "${local.traefik.web_port}"
      NOMAD_VAR_traefik_api_port = "${local.traefik.api_port}"
    }
  }
}

build {
  name        = "ors:deploy"
  description = "Deploys an instance of ORS to a Nomad cluster with support for automatically fetching .osm.pbf file updates." 
  sources     = ["null.null"]

  provisioner "ansible" {
    playbook_file   = "/project/ansible/playbook.yml"
    use_proxy       = "true"
    extra_arguments = [
      "-e packer_vars_json=${jsonencode(local.ors)}",
      "-e nomad_host=${var.nomad_host}",
      "-e nomad_port=${var.nomad_port}",
      "-e docker_volume_root=/project",
      "--tags=${var.ansible_playbook_tags}"
    ]
  }
}