# global
variable "project_dir" {
  type        = string
  description = "The absolute path to the project root directory."
}
#>

# nomad cluster
variable "nomad_cluster_address" {
  type        = string
  description = "The address to communcate with the nomad cluster."
  default     = "http://localhost:4646" 
}

# traefik
variable "traefik_web_port" {
  type        = number
  description = "The host port the traefik web proxy should bind to."
  default     = 8085
}
variable "traefik_api_port" {
  type        = number
  description = "The host port the traefik api service should bind to."
  default     = 8086
}
#>

# ansible
variable "nomad_host" {
  type        = string
  description = "The IP address where the Nomad cluster can be reached."
  default     = "host.docker.internal"
}
variable "nomad_port" {
  type        = number
  description = "The port where the Nomad cluster can be reached."
  default     = 4646
}
variable "ansible_playbook_tags" {
  type        = string
  description = "The tags used to specify the ansible task to run. Supported tags: [ all, nomad_ors_datasource_template, nomad_ors_api_datasource_template, nomad_template, nomad_deploy ]"
  default = "all"
}
#>

# ors
variable "ors_docker_image_tag" {
  type        = string
  description = "ORS docker image tag."
  default     = "v8.0.0"
}
variable "ors_datasources" {
  type       = list(object({
    name     = string
    pbf_path = string
    api = object({
      update_min_healthy_time  = string
      update_healthy_deadline  = string
      update_progress_deadline = string
      update_auto_revert       = bool
      update_auto_promote      = bool

      traefik_host = string

      resources_cpu    = number
      resources_memory = number

      ors_jvm_xms = string
      ors_jvm_xmx = string

      ors_config = list(object({
        key   = string
        value = string
      }))
    })
  }))
  description  = "Configuration object to manage datasources to deploy."
  default      = [
    {
      name     = "new-york"
      pbf_path = "north-america/us/new-york-latest"
      api      = {
        update_min_healthy_time  = "30s"
        update_healthy_deadline  = "20m"
        update_progress_deadline = "25m"
        update_auto_revert       = true
        update_auto_promote      = true
        traefik_host             = "new-york.ors.test"
        resources_cpu            = 3500 # 3.5Ghz
        resources_memory         = 4000 # 4Gb
        ors_jvm_xms              = "1g"
        ors_jvm_xmx              = "2g"
        ors_config               = [
          { key = "ors.engine.profiles.car.elevation", value = "False"}
        ]
      }
    }
  ]
}
variable "ors_datasource_cron" {
  type      = object({
    enabled = bool
    cron    = string
  })
  description = "Object to manage the renewal interval to check for updated .osm.pbf files."
  default     = {
    enabled   = true
    cron      = "@weekly"
  }
}
#>

# constants
locals {
  traefik = {
    web_port = "${var.traefik_web_port}"
    api_port = "${var.traefik_api_port}"
  }
}
locals {
  ors = {
    # pip
    pip_data_dir = "${var.project_dir}/data/pip"

    # docker
    docker_tag = "${var.ors_docker_image_tag}"

    # nomad
    nomad_api_job_spec_dir = "${var.project_dir}/nomad/jobs/ors-api-datasource"

    # datasource
    pbf_data_dir     = "${var.project_dir}/data/pbf"
    pbf_fetch_domain = "http://download.geofabrik.de"
    datasources      = "${var.ors_datasources}"
    datasource_cron  = "${var.ors_datasource_cron}"
  }
}