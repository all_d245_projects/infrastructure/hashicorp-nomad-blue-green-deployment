# traefik config
traefik_web_port = 8085
traefik_api_port = 8086
#>

# ors config
ors_datasources = [
  {
    name     = "new-york"
    pbf_path = "north-america/us/new-york-240730"
    api      = {
      update_min_healthy_time  = "30s"
      update_healthy_deadline  = "20m"
      update_progress_deadline = "25m"
      update_auto_revert       = true
      update_auto_promote      = false
      traefik_host             = "new-york.ors.test"
      resources_cpu            = 3500 # 3.5Ghz
      resources_memory         = 4000 # 4Gb
      ors_jvm_xms              = "1g"
      ors_jvm_xmx              = "2g"
      ors_config               = [
        { key = "ors.engine.profiles.car.elevation", value = "False"}
      ]
    }
  }
]
#>